/* fso-gpsd - a gpsd compatibility daemon for the FSO frameworkd
 *
 * Copyright (C) 2008 Sascha Wessel <wessel@nefkom.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <netdb.h>
#include <math.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <dbus/dbus-glib.h>
#include <glib-object.h>
#include <glib.h>

#define KNOTS_TO_KPH (1.852)
#define KNOTS_TO_MPS (0.51444444)

/******************************************************************************/

#define ERROR(...)							\
	do {								\
		g_log(NULL, G_LOG_LEVEL_ERROR, __VA_ARGS__);		\
		abort();						\
	} while (0)

#define CRITICAL(...)							\
	do {								\
		if (G_UNLIKELY(log_level & G_LOG_LEVEL_CRITICAL)) {	\
			g_log(NULL, G_LOG_LEVEL_CRITICAL, __VA_ARGS__);	\
		}							\
	} while (0)

#define WARNING(...)							\
	do {								\
		if (G_UNLIKELY(log_level & G_LOG_LEVEL_WARNING)) {	\
			g_log(NULL, G_LOG_LEVEL_WARNING, __VA_ARGS__);	\
		}							\
	} while (0)

#define MESSAGE(...)							\
	do {								\
		if (G_UNLIKELY(log_level & G_LOG_LEVEL_MESSAGE)) {	\
			g_log(NULL, G_LOG_LEVEL_MESSAGE, __VA_ARGS__);	\
		}							\
	} while (0)

#define INFO(...)							\
	do {								\
		if (G_UNLIKELY(log_level & G_LOG_LEVEL_INFO)) {		\
			g_log(NULL, G_LOG_LEVEL_INFO, __VA_ARGS__);	\
		}							\
	} while (0)

#define DEBUG(...)							\
	do {								\
		if (G_UNLIKELY(log_level & G_LOG_LEVEL_DEBUG)) {	\
			g_log(NULL, G_LOG_LEVEL_DEBUG, __VA_ARGS__);	\
		}							\
	} while (0)

/******************************************************************************/

#define POSITION_LATITUDE_IS_VALID	(position_fields & (1 << 0))
#define POSITION_LONGITUDE_IS_VALID	(position_fields & (1 << 1))
#define POSITION_ALTITUDE_IS_VALID	(position_fields & (1 << 2))

#define ACCURACY_PDOP_IS_VALID		(accuracy_fields & (1 << 0))
#define ACCURACY_HDOP_IS_VALID		(accuracy_fields & (1 << 1))
#define ACCURACY_VDOP_IS_VALID		(accuracy_fields & (1 << 2))

#define COURSE_SPEED_IS_VALID		(course_fields & (1 << 0))
#define COURSE_DIRECTION_IS_VALID	(course_fields & (1 << 1))
#define COURSE_CLIMB_IS_VALID		(course_fields & (1 << 2))

/******************************************************************************/

struct client {
	GIOChannel *channel;
	gboolean watcher;
	gboolean raw;
};

struct satellite {
	unsigned prn;
	gboolean used;
	unsigned elevation;
	unsigned azimuth;
	unsigned snr;
};

/******************************************************************************/

static int log_level = G_LOG_LEVEL_ERROR
		     | G_LOG_LEVEL_CRITICAL
		     | G_LOG_LEVEL_WARNING;

static gboolean background = FALSE;
static gboolean nowait = FALSE;

/******************************************************************************/

static gboolean info_queued = FALSE;
static gboolean sats_queued = FALSE;
static gboolean nmea_queued = FALSE;

/******************************************************************************/

static gboolean timeout_handler_registered = FALSE;

/******************************************************************************/

static GList *client_list = NULL;

/******************************************************************************/

static DBusGConnection *connection = NULL;

/******************************************************************************/

static const char *gypsy_device = "";
static const char *gypsy_service = "org.freedesktop.Gypsy";
static const char *gypsy_server_path = "/org/freedesktop/Gypsy";
static char *gypsy_device_path = NULL;

static gboolean gypsy_device_create_failed = FALSE;
static gboolean gypsy_device_start_failed = FALSE;

/******************************************************************************/

#define GYPSY_SERVER "org.freedesktop.Gypsy.Server"
static DBusGProxy *server_proxy = NULL;

#define GYPSY_DEVICE "org.freedesktop.Gypsy.Device"
static DBusGProxy *device_proxy = NULL;
static gboolean device_connectionstatus = 0;
static int device_fixstatus = 0; /* 0=INVALID, 1=NONE, 2=2D, 3=3D */

#define GYPSY_TIME "org.freedesktop.Gypsy.Time"
static DBusGProxy *time_proxy = NULL;
static int time_seconds = 0;

#define GYPSY_POSITION "org.freedesktop.Gypsy.Position"
static DBusGProxy *position_proxy = NULL;
static int position_fields = 0;
static int position_timestamp = 0;
static double position_latitude = 0.0;
static double position_longitude = 0.0;
static double position_altitude = 0.0;

#define GYPSY_ACCURACY "org.freedesktop.Gypsy.Accuracy"
static DBusGProxy *accuracy_proxy = NULL;
static int accuracy_fields = 0;
static double accuracy_pdop = 0.0;
static double accuracy_hdop = 0.0;
static double accuracy_vdop = 0.0;

#define GYPSY_COURSE "org.freedesktop.Gypsy.Course"
static DBusGProxy *course_proxy = NULL;
static int course_fields = 0;
static int course_timestamp = 0;
static double course_speed = 0.0;
static double course_direction = 0.0;
static double course_climb = 0.0;

#define GYPSY_SATELLITE "org.freedesktop.Gypsy.Satellite"
static DBusGProxy *satellite_proxy = NULL;
#define SATELLITEC_MAX 12
static struct satellite satellite_satellitev[SATELLITEC_MAX];
static unsigned satellite_satellitec = 0;

/******************************************************************************/

static GString *nmea_gga;
static GString *nmea_gsa;
static GString *nmea_gsv[3];
static GString *nmea_rmc;
static GString *nmea_gll;
static GString *nmea_vtg;

/******************************************************************************/

static int
get_fixstatus(void)
{
	if (POSITION_LATITUDE_IS_VALID && POSITION_LONGITUDE_IS_VALID) {
		if (POSITION_ALTITUDE_IS_VALID) {
			return MIN(device_fixstatus, 3);
		} else {
			return MIN(device_fixstatus, 2);
		}
	} else {
		return MIN(device_fixstatus, 1);
	}
}

static gboolean
check_period(const GTimeVal *tv1, const GTimeVal *tv2, glong sec)
{
	glong tv1_sec = tv1->tv_sec + sec;
	glong tv1_usec = tv1->tv_usec;
	glong tv2_sec = tv2->tv_sec;
	glong tv2_usec = tv2->tv_usec;

	if (tv1_sec == tv2_sec) {
		return (tv1_usec > tv2_usec);
	} else {
		return (tv1_sec > tv2_sec);
	}
}

/******************************************************************************/

static void
shutdown_client(struct client *client)
{
	if (client->channel) {
		MESSAGE("Shutdown client");

		g_io_channel_shutdown(client->channel, FALSE, NULL);
	}

	client->channel = NULL;
	client->watcher = FALSE;
	client->raw = FALSE;
}

static void
send_to_client(struct client *client, const char *string, gsize len)
{
	GIOStatus status;
	gsize bytes_written;

	if (!client->channel) {
		return;
	}

	status = g_io_channel_write_chars(client->channel, string,
		len, &bytes_written, NULL);

	if ((bytes_written != len) || (status != G_IO_STATUS_NORMAL)) {
		shutdown_client(client);
	}
}

/******************************************************************************/

static void
add_nmea_checksum(GString *nmea)
{
	int checksum = 0;
	char *p;

	p = nmea->str;

	if (*p == '$') {
		p++;
	}

	while (*p) {
		checksum ^= (int) *p;
		p++;
	}

	g_string_append_printf(nmea, "*%02x", checksum);
}

static void
add_nmea_time(GString *nmea)
{
	if (time_seconds <= 0) {
		g_string_append_c(nmea, ',');
	} else {
		struct tm *tm;
		time_t seconds;

		seconds = time_seconds;
		tm = gmtime(&seconds);

		g_string_append_printf(nmea, ",%02d%02d%02d.00",
			tm->tm_hour, tm->tm_min, tm->tm_sec);
	}
}

static void
add_nmea_latitude(GString *nmea)
{
	if (!POSITION_LATITUDE_IS_VALID) {
		g_string_append_c(nmea, ',');
		g_string_append_c(nmea, ',');
	} else {
		double a, d;

		a = fabs(position_latitude);
		d = floor(a);

		g_string_append_printf(nmea, ",%010.5f,%c",
			(d * 100.0) + ((a - d) * 60.0),
			(position_latitude > 0) ? 'N' : 'S');
	}
}

static void
add_nmea_longitude(GString *nmea)
{
	if (!POSITION_LONGITUDE_IS_VALID) {
		g_string_append_c(nmea, ',');
		g_string_append_c(nmea, ',');
	} else {
		double a, d;

		a = fabs(position_longitude);
		d = floor(a);

		g_string_append_printf(nmea, ",%011.5f,%c",
			(d * 100.0) + ((a - d) * 60.0),
			(position_longitude > 0) ? 'E' : 'W');
	}
}

/******************************************************************************/

static void
update_nmea_gga(void)
{
	unsigned i, used;

	g_string_assign(nmea_gga, "$GPGGA");

	add_nmea_time(nmea_gga);

	add_nmea_latitude(nmea_gga);

	add_nmea_longitude(nmea_gga);

	g_string_append_printf(nmea_gga, ",%1d", MAX(get_fixstatus() - 1, 0));

	for (i = 0, used = 0; i < satellite_satellitec; i++) {
		if (satellite_satellitev[i].used) {
			used++;
		}
	}

	g_string_append_printf(nmea_gga, ",%02d", used);

	if (!ACCURACY_HDOP_IS_VALID) {
		g_string_append_c(nmea_gga, ',');
	} else {
		g_string_append_printf(nmea_gga, ",%04.2f", accuracy_hdop);
	}

	if (!POSITION_ALTITUDE_IS_VALID) {
		g_string_append_c(nmea_gga, ',');
		g_string_append_c(nmea_gga, ',');
	} else {
		g_string_append_printf(nmea_gga, ",%03.1f,M",
			position_altitude);
	}

	g_string_append_c(nmea_gga, ','); /* FIXME */
	g_string_append_c(nmea_gga, ',');
	g_string_append_c(nmea_gga, ',');
	g_string_append_c(nmea_gga, ',');

	add_nmea_checksum(nmea_gga);
}

static void
update_nmea_gsa(void)
{
	unsigned i;

	g_string_assign(nmea_gsa, "$GPGSA");

	g_string_append_c(nmea_gsa, ',');
	g_string_append_c(nmea_gsa, 'A');

	g_string_append_printf(nmea_gsa, ",%1d", MAX(get_fixstatus() - 1, 0));

	for (i = 0; i < 12; i++) {
		if (i < satellite_satellitec) {
			g_string_append_printf(nmea_gsa, ",%02d",
				satellite_satellitev[i].prn);
		} else {
			g_string_append_c(nmea_gsa, ',');
		}
	}

	if (!ACCURACY_PDOP_IS_VALID) {
		g_string_append_c(nmea_gsa, ',');
	} else {
		g_string_append_printf(nmea_gsa, ",%04.2f", accuracy_pdop);
	}

	if (!ACCURACY_HDOP_IS_VALID) {
		g_string_append_c(nmea_gsa, ',');
	} else {
		g_string_append_printf(nmea_gsa, ",%04.2f", accuracy_hdop);
	}

	if (!ACCURACY_VDOP_IS_VALID) {
		g_string_append_c(nmea_gsa, ',');
	} else {
		g_string_append_printf(nmea_gsa, ",%04.2f", accuracy_vdop);
	}

	add_nmea_checksum(nmea_gsa);
}

static void
update_nmea_gsv(void)
{
	unsigned i;
	unsigned sat_count;
	unsigned sentence_count;

	g_string_truncate(nmea_gsv[0], 0);
	g_string_truncate(nmea_gsv[1], 0);
	g_string_truncate(nmea_gsv[2], 0);

	if (satellite_satellitec == 0) {
		g_string_assign(nmea_gsv[0], "$GPGSV,1,1,00*79");
		return;
	}

	sat_count = MIN(satellite_satellitec, 12);
	sentence_count = ((sat_count - 1) / 4) + 1;

	for (i = 0; i < sat_count; i++) {
		int index_div, index_mod;

		index_div = i / 4;
		index_mod = i % 4;

		if (index_mod == 0) {
			g_string_append_printf(nmea_gsv[index_div],
				"$GPGSV,%1d,%1d,%02d",
				sentence_count,
				index_div + 1,
				sat_count);
		}

		g_string_append_printf(nmea_gsv[index_div],
			",%02u,%02u,%03u,%02u",
			satellite_satellitev[i].prn,
			satellite_satellitev[i].elevation,
			satellite_satellitev[i].azimuth,
			satellite_satellitev[i].snr);
	}

	for (i = 0; i < 3; i++) {
		if (nmea_gsv[i]->len > 0) {
			add_nmea_checksum(nmea_gsv[i]);
		}
	}
}

static void
update_nmea_rmc(void)
{
	g_string_assign(nmea_rmc, "$GPRMC");

	add_nmea_time(nmea_rmc);

	if (get_fixstatus() > 1) {
		g_string_append_c(nmea_rmc, ',');
		g_string_append_c(nmea_rmc, 'A');
	} else {
		g_string_append_c(nmea_rmc, ',');
		g_string_append_c(nmea_rmc, 'V');
	}

	add_nmea_latitude(nmea_rmc);

	add_nmea_longitude(nmea_rmc);

	if (!COURSE_SPEED_IS_VALID) {
		g_string_append_c(nmea_rmc, ',');
	} else {
		g_string_append_printf(nmea_rmc, ",%07.3f",
			course_speed);
	}

	if (!COURSE_DIRECTION_IS_VALID) {
		g_string_append_c(nmea_rmc, ',');
	} else {
		g_string_append_printf(nmea_rmc, ",%06.2f",
			course_direction);
	}

	if (time_seconds <= 0) {
		g_string_append_c(nmea_rmc, ',');
	} else {
		struct tm *tm;
		time_t seconds;

		seconds = time_seconds;
		tm = gmtime(&seconds);

		g_string_append_printf(nmea_rmc, ",%02d%02d%02d",
			tm->tm_mday, tm->tm_mon + 1, tm->tm_year % 100);
	}

	g_string_append_c(nmea_rmc, ',');
	g_string_append_c(nmea_rmc, ',');

	add_nmea_checksum(nmea_rmc);
}

static void
update_nmea_gll(void)
{
	g_string_assign(nmea_gll, "$GPGLL");

	add_nmea_latitude(nmea_gll);

	add_nmea_longitude(nmea_gll);

	add_nmea_time(nmea_gll);

	if (get_fixstatus() > 1) {
		g_string_append_c(nmea_gll, ',');
		g_string_append_c(nmea_gll, 'A');
	} else {
		g_string_append_c(nmea_gll, ',');
		g_string_append_c(nmea_gll, 'V');
	}

	add_nmea_checksum(nmea_gll);
}

static void
update_nmea_vtg(void)
{
	g_string_assign(nmea_vtg, "$GPVTG");

	if (!COURSE_DIRECTION_IS_VALID) {
		g_string_append_c(nmea_vtg, ',');
		g_string_append_c(nmea_vtg, ',');
	} else {
		g_string_append_printf(nmea_vtg, ",%04.2f,T", course_direction);
	}

	g_string_append_c(nmea_vtg, ','); /* FIXME */
	g_string_append_c(nmea_vtg, ',');

	if (!COURSE_SPEED_IS_VALID) {
		g_string_append_c(nmea_vtg, ',');
		g_string_append_c(nmea_vtg, ',');
		g_string_append_c(nmea_vtg, ',');
		g_string_append_c(nmea_vtg, ',');
	} else {
		g_string_append_printf(nmea_vtg, ",%05.3f,N,%05.3f,K",
			course_speed, course_speed * KNOTS_TO_KPH);
	}

	add_nmea_checksum(nmea_vtg);
}

/******************************************************************************/

static gboolean
send_nmea_to_clients(gpointer data)
{
	GString *string;
	GList *l;

	for (string = NULL, l = client_list; l; l = l->next) {
		struct client *client = l->data;

		if (client->raw) {
			if (!string) {
				static GTimeVal timestamp = { 0, 0 };

				if (data && check_period(&timestamp, data, 1)) {
					return FALSE;
				}

				g_get_current_time(&timestamp);

				string = g_string_sized_new(1024);

				if (!nmea_gga->len) {
					update_nmea_gga();
				}
				if (!nmea_gsa->len) {
					update_nmea_gsa();
				}
				if (!nmea_gsv[0]->len) {
					update_nmea_gsv();
				}
				if (!nmea_rmc->len) {
					update_nmea_rmc();
				}
				if (!nmea_gll->len) {
					update_nmea_gll();
				}
				if (!nmea_vtg->len) {
					update_nmea_vtg();
				}

				if (nmea_gga->len) {
					INFO("Sending nmea: %s",
						nmea_gga->str);
					g_string_append_len(string,
						nmea_gga->str,
						nmea_gga->len);
					g_string_append_c(string, '\r');
					g_string_append_c(string, '\n');
				}
				if (nmea_gsa->len) {
					INFO("Sending nmea: %s",
						nmea_gsa->str);
					g_string_append_len(string,
						nmea_gsa->str,
						nmea_gsa->len);
					g_string_append_c(string, '\r');
					g_string_append_c(string, '\n');
				}
				if (nmea_gsv[0]->len) {
					INFO("Sending nmea: %s",
						nmea_gsv[0]->str);
					g_string_append_len(string,
						nmea_gsv[0]->str,
						nmea_gsv[0]->len);
					g_string_append_c(string, '\r');
					g_string_append_c(string, '\n');
				}
				if (nmea_gsv[1]->len) {
					INFO("Sending nmea: %s",
						nmea_gsv[1]->str);
					g_string_append_len(string,
						nmea_gsv[1]->str,
						nmea_gsv[1]->len);
					g_string_append_c(string, '\r');
					g_string_append_c(string, '\n');
				}
				if (nmea_gsv[2]->len) {
					INFO("Sending nmea: %s",
						nmea_gsv[2]->str);
					g_string_append_len(string,
						nmea_gsv[2]->str,
						nmea_gsv[2]->len);
					g_string_append_c(string, '\r');
					g_string_append_c(string, '\n');
				}
				if (nmea_rmc->len) {
					INFO("Sending nmea: %s",
						nmea_rmc->str);
					g_string_append_len(string,
						nmea_rmc->str,
						nmea_rmc->len);
					g_string_append_c(string, '\r');
					g_string_append_c(string, '\n');
				}
				if (nmea_gll->len) {
					INFO("Sending nmea: %s",
						nmea_gll->str);
					g_string_append_len(string,
						nmea_gll->str,
						nmea_gll->len);
					g_string_append_c(string, '\r');
					g_string_append_c(string, '\n');
				}
				if (nmea_vtg->len) {
					INFO("Sending nmea: %s",
						nmea_vtg->str);
					g_string_append_len(string,
						nmea_vtg->str,
						nmea_vtg->len);
					g_string_append_c(string, '\r');
					g_string_append_c(string, '\n');
				}
			}

			send_to_client(client, string->str, string->len);
		}
	}

	if (string) {
		g_string_free(string, TRUE);
	}

	nmea_queued = FALSE;

	return FALSE;
}

static void
queue_send_nmea_to_clients(void)
{
	if (nmea_queued) {
		return;
	}

	nmea_queued = TRUE;

	g_idle_add(send_nmea_to_clients, NULL);
}

/******************************************************************************/

static void
add_sats(GString *string)
{
	unsigned i, count;

	if (satellite_satellitec == 0) {
		g_string_append(string, ",Y=?");
		return;
	}

	g_string_append(string, ",Y=-");

	if (time_seconds <= 0) {
		g_string_append(string, " ? ");
	} else {
		g_string_append_printf(string, " %d.000 ", time_seconds);
	}

	count = MIN(satellite_satellitec, 12);

	g_string_append_printf(string, "%d:", count);

	for (i = 0; i < count; i++) {
		g_string_append_printf(string, "%d %d %d %d %d:",
			satellite_satellitev[i].prn,
			satellite_satellitev[i].elevation,
			satellite_satellitev[i].azimuth,
			satellite_satellitev[i].snr,
			satellite_satellitev[i].used);
	}
}

/******************************************************************************/

static gboolean
send_sats_to_clients(gpointer data)
{
	GString *string;
	GList *l;

	for (string = NULL, l = client_list; l; l = l->next) {
		struct client *client = l->data;

		if (client->watcher) {
			if (!string) {
				static GTimeVal timestamp = { 0, 0 };

				if (data && check_period(&timestamp, data, 1)) {
					return FALSE;
				}

				g_get_current_time(&timestamp);

				string = g_string_sized_new(1024);

				g_string_append(string, "GPSD");

				add_sats(string);

				INFO("Sending sats: %s", string->str);

				g_string_append_c(string, '\r');
				g_string_append_c(string, '\n');
			}

			send_to_client(client, string->str, string->len);
		}
	}

	if (string) {
		g_string_free(string, TRUE);
	}

	sats_queued = FALSE;

	return FALSE;
}

static void
queue_send_sats_to_clients(void)
{
	if (sats_queued) {
		return;
	}

	sats_queued = TRUE;

	g_idle_add(send_sats_to_clients, NULL);
}

/******************************************************************************/

static void
add_info(GString *string)
{
	if (!device_connectionstatus || get_fixstatus() <= 1) {
		g_string_append(string, ",O=?");
		return;
	}

	g_string_append(string, ",O=-");

	if (time_seconds <= 0) {
		g_string_append_c(string, ' ');
		g_string_append_c(string, '?');
	} else {
		g_string_append_printf(string, " %d.000", time_seconds);
	}

	g_string_append_c(string, ' '); /* FIXME */
	g_string_append_c(string, '?');

	if (!POSITION_LATITUDE_IS_VALID) {
		g_string_append_c(string, ' ');
		g_string_append_c(string, '?');
	} else {
		g_string_append_printf(string, " %.6f", position_latitude);
	}
	if (!POSITION_LONGITUDE_IS_VALID) {
		g_string_append_c(string, ' ');
		g_string_append_c(string, '?');
	} else {
		g_string_append_printf(string, " %.6f", position_longitude);
	}
	if (!POSITION_ALTITUDE_IS_VALID) {
		g_string_append_c(string, ' ');
		g_string_append_c(string, '?');
	} else {
		g_string_append_printf(string, " %.2f", position_altitude);
	}
	if (!ACCURACY_HDOP_IS_VALID) {
		g_string_append_c(string, ' ');
		g_string_append_c(string, '?');
	} else {
		g_string_append_printf(string, " %.2f", accuracy_hdop);
	}
	if (!ACCURACY_VDOP_IS_VALID) {
		g_string_append_c(string, ' ');
		g_string_append_c(string, '?');
	} else {
		g_string_append_printf(string, " %.2f", accuracy_vdop);
	}
	if (!COURSE_DIRECTION_IS_VALID) {
		g_string_append_c(string, ' ');
		g_string_append_c(string, '?');
	} else {
		g_string_append_printf(string, " %.4f", course_direction);
	}
	if (!COURSE_SPEED_IS_VALID) {
		g_string_append_c(string, ' ');
		g_string_append_c(string, '?');
	} else {
		g_string_append_printf(string, " %.3f",
			course_speed * KNOTS_TO_MPS);
	}
	if (!COURSE_CLIMB_IS_VALID) {
		g_string_append_c(string, ' ');
		g_string_append_c(string, '?');
	} else {
		g_string_append_printf(string, " %.3f", course_climb);
	}

	g_string_append(string, " ? ? ?"); /* FIXME */

	g_string_append_printf(string, " %d", get_fixstatus());
}

/******************************************************************************/

static gboolean
send_info_to_clients(gpointer data)
{
	GString *string;
	GList *l;

	for (string = NULL, l = client_list; l; l = l->next) {
		struct client *client = l->data;

		if (client->watcher) {
			if (!string) {
				static GTimeVal timestamp = { 0, 0 };

				if (data && check_period(&timestamp, data, 1)) {
					return FALSE;
				}

				g_get_current_time(&timestamp);

				string = g_string_sized_new(1024);

				g_string_append(string, "GPSD");

				add_info(string);

				INFO("Sending info: %s", string->str);

				g_string_append_c(string, '\r');
				g_string_append_c(string, '\n');
			}

			send_to_client(client, string->str, string->len);
		}
	}

	if (string) {
		g_string_free(string, TRUE);
	}

	info_queued = FALSE;

	return FALSE;
}

static void
queue_send_info_to_clients(void)
{
	if (info_queued) {
		return;
	}

	info_queued = TRUE;

	g_idle_add(send_info_to_clients, NULL);
}

/******************************************************************************/

static void
connection_status_changed(
	DBusGProxy *proxy,
	gboolean connectionstatus,
	gpointer user_data
)
{
	device_connectionstatus = connectionstatus;

	INFO(GYPSY_DEVICE": ConnectionStatusChanged(connectionstatus=%s)",
		device_connectionstatus ? "TRUE" : "FALSE");
}

static void
fix_status_changed(
	DBusGProxy *proxy,
	int fixstatus,
	gpointer user_data
)
{
	const char *str;

	device_fixstatus = fixstatus;

	switch (device_fixstatus) {
	case 1:
		str = "NONE";
		break;
	case 2:
		str = "2D";
		break;
	case 3:
		str = "3D";
		break;
	default:
		str = "INVALID";
		break;
	}

	INFO(GYPSY_DEVICE": FixStatusChanged(fixstatus=%s)", str);

	g_string_truncate(nmea_gga, 0);
	g_string_truncate(nmea_gsa, 0);
	g_string_truncate(nmea_rmc, 0);
	g_string_truncate(nmea_gll, 0);

	queue_send_nmea_to_clients();
	queue_send_info_to_clients();
}

static void
time_changed(
	DBusGProxy *proxy,
	int seconds,
	gpointer user_data
)
{
	time_seconds = seconds;

	INFO(GYPSY_TIME": TimeChanged(seconds=%d)", time_seconds);

	g_string_truncate(nmea_gga, 0);
	g_string_truncate(nmea_rmc, 0);
	g_string_truncate(nmea_gll, 0);

	queue_send_nmea_to_clients();
	queue_send_info_to_clients();
}

static void
position_changed(
	DBusGProxy *proxy,
	int fields,
	int timestamp,
	double latitude,
	double longitude,
	double altitude,
	gpointer user_data
)
{
	position_fields    = fields;
	position_timestamp = timestamp;
	position_latitude  = latitude;
	position_longitude = longitude;
	position_altitude  = altitude;

	INFO(GYPSY_POSITION": PositionChanged(timestamp=%d, latitude=%f, longitude=%f, altitude=%f)",
		position_timestamp,
		POSITION_LATITUDE_IS_VALID  ? position_latitude  : NAN,
		POSITION_LONGITUDE_IS_VALID ? position_longitude : NAN,
		POSITION_ALTITUDE_IS_VALID  ? position_altitude  : NAN);

	g_string_truncate(nmea_gga, 0);
	g_string_truncate(nmea_rmc, 0);
	g_string_truncate(nmea_gll, 0);

	queue_send_nmea_to_clients();
	queue_send_info_to_clients();
}

static void
accuracy_changed(
	DBusGProxy *proxy,
	int fields,
	double pdop,
	double hdop,
	double vdop,
	gpointer user_data
)
{
	accuracy_fields = fields;
	accuracy_pdop   = pdop;
	accuracy_hdop   = hdop;
	accuracy_vdop   = vdop;

	INFO(GYPSY_ACCURACY": AccuracyChanged(pdop=%f, hdop=%f, vdop=%f)",
		ACCURACY_PDOP_IS_VALID ? accuracy_pdop : NAN,
		ACCURACY_HDOP_IS_VALID ? accuracy_hdop : NAN,
		ACCURACY_VDOP_IS_VALID ? accuracy_vdop : NAN);

	g_string_truncate(nmea_gga, 0);
	g_string_truncate(nmea_gsa, 0);

	queue_send_nmea_to_clients();
	queue_send_info_to_clients();
}

static void
course_changed(
	DBusGProxy *proxy,
	int fields,
	int timestamp,
	double speed,
	double direction,
	double climb,
	gpointer user_data
)
{
	course_fields    = fields;
	course_timestamp = timestamp;
	course_speed     = speed;
	course_direction = direction;
	course_climb     = climb;

	INFO(GYPSY_COURSE": CourseChanged(timestamp=%d, speed=%f, direction=%f, climb=%f)",
		course_timestamp,
		COURSE_SPEED_IS_VALID     ? course_speed     : NAN,
		COURSE_DIRECTION_IS_VALID ? course_direction : NAN,
		COURSE_CLIMB_IS_VALID     ? course_climb     : NAN);

	g_string_truncate(nmea_rmc, 0);
	g_string_truncate(nmea_vtg, 0);

	queue_send_nmea_to_clients();
	queue_send_info_to_clients();
}

static void
satellites_changed(
	DBusGProxy *proxy,
	GPtrArray *satellites,
	gpointer user_data
)
{
	int i;

	if (!satellites) {
		satellite_satellitec = 0;
	} else {
		satellite_satellitec = MIN(SATELLITEC_MAX, satellites->len);
	}

	for (i = 0; i < satellite_satellitec; i++) {
		struct satellite *sat;
		GValueArray *val;

		sat = satellite_satellitev + i;
		val = satellites->pdata[i];

		sat->prn = g_value_get_uint(g_value_array_get_nth(val, 0));
		sat->used = g_value_get_boolean(g_value_array_get_nth(val, 1));
		sat->elevation = g_value_get_uint(g_value_array_get_nth(val, 2));
		sat->azimuth = g_value_get_uint(g_value_array_get_nth(val, 3));
		sat->snr = g_value_get_uint(g_value_array_get_nth(val, 4));

		INFO(GYPSY_SATELLITE": SatellitesChanged(index=%d, prn=%u, used=%s, elevation=%u, azimuth=%u, snr=%u)",
			i,
			sat->prn,
			sat->used ? "TRUE" : "FALSE",
			sat->elevation,
			sat->azimuth,
			sat->snr);
	}

	g_string_truncate(nmea_gga, 0);
	g_string_truncate(nmea_gsa, 0);
	g_string_truncate(nmea_gsv[0], 0);
	g_string_truncate(nmea_gsv[1], 0);
	g_string_truncate(nmea_gsv[2], 0);

	queue_send_nmea_to_clients();
	queue_send_sats_to_clients();
}

/******************************************************************************/

static void
get_connection_status_notify(DBusGProxy *proxy, DBusGProxyCall *call, gpointer data)
{
	GError *error = NULL;
	gboolean connectionstatus = FALSE;

	DEBUG(GYPSY_DEVICE": GetConnectionStatus: Notify");

	dbus_g_proxy_end_call(proxy, call, &error,
		G_TYPE_BOOLEAN, &connectionstatus,
		G_TYPE_INVALID);

	if (error) {
		WARNING(GYPSY_DEVICE": GetConnectionStatus: %s", error->message);
		g_error_free(error);
	} else {
		DEBUG(GYPSY_DEVICE": GetConnectionStatus: OK");

		if (proxy == device_proxy) {
			connection_status_changed(NULL, connectionstatus, NULL);
		}
	}
}

static void
get_fix_status_notify(DBusGProxy *proxy, DBusGProxyCall *call, gpointer data)
{
	GError *error = NULL;
	int fixstatus = 0;

	DEBUG(GYPSY_DEVICE": GetFixStatus: Notify");

	dbus_g_proxy_end_call(proxy, call, &error,
		G_TYPE_INT, &fixstatus,
		G_TYPE_INVALID);

	if (error) {
		WARNING(GYPSY_DEVICE": GetFixStatus: %s", error->message);
		g_error_free(error);
	} else {
		DEBUG(GYPSY_DEVICE": GetFixStatus: OK");

		if (proxy == device_proxy) {
			fix_status_changed(NULL, fixstatus, NULL);
		}
	}
}

static void
get_time_notify(DBusGProxy *proxy, DBusGProxyCall *call, gpointer data)
{
	GError *error = NULL;
	int seconds = 0;

	DEBUG(GYPSY_TIME": GetTime: Notify");

	dbus_g_proxy_end_call(proxy, call, &error,
		G_TYPE_INT, &seconds,
		G_TYPE_INVALID);

	if (error) {
		WARNING(GYPSY_TIME": GetTime: %s", error->message);
		g_error_free(error);
	} else {
		DEBUG(GYPSY_TIME": GetTime: OK");

		if (proxy == time_proxy) {
			time_changed(NULL, seconds, NULL);
		}
	}
}

static void
get_position_notify(DBusGProxy *proxy, DBusGProxyCall *call, gpointer data)
{
	GError *error = NULL;
	int fields = 0, timestamp = 0;
	double latitude = 0.0, longitude = 0.0, altitude = 0.0;

	DEBUG(GYPSY_POSITION": GetPosition: Notify");

	dbus_g_proxy_end_call(proxy, call, &error,
		G_TYPE_INT, &fields,
		G_TYPE_INT, &timestamp,
		G_TYPE_DOUBLE, &latitude,
		G_TYPE_DOUBLE, &longitude,
		G_TYPE_DOUBLE, &altitude,
		G_TYPE_INVALID);

	if (error) {
		WARNING(GYPSY_POSITION": GetPosition: %s", error->message);
		g_error_free(error);
	} else {
		DEBUG(GYPSY_POSITION": GetPosition: OK");

		if (proxy == position_proxy) {
			position_changed(NULL, fields, timestamp, latitude,
				longitude, altitude, NULL);
		}
	}
}

static void
get_accuracy_notify(DBusGProxy *proxy, DBusGProxyCall *call, gpointer data)
{
	GError *error = NULL;
	int fields = 0;
	double pdop = 0.0, hdop = 0.0, vdop = 0.0;

	DEBUG(GYPSY_ACCURACY": GetAccuracy: Notify");

	dbus_g_proxy_end_call(proxy, call, &error,
		G_TYPE_INT, &fields,
		G_TYPE_DOUBLE, &pdop,
		G_TYPE_DOUBLE, &hdop,
		G_TYPE_DOUBLE, &vdop,
		G_TYPE_INVALID);

	if (error) {
		WARNING(GYPSY_ACCURACY": GetAccuracy: %s", error->message);
		g_error_free(error);
	} else {
		DEBUG(GYPSY_ACCURACY": GetAccuracy: OK");

		if (proxy == accuracy_proxy) {
			accuracy_changed(NULL, fields, pdop, hdop, vdop, NULL);
		}
	}
}

static void
get_course_notify(DBusGProxy *proxy, DBusGProxyCall *call, gpointer data)
{
	GError *error = NULL;
	int fields = 0, timestamp = 0;
	double speed = 0.0, direction = 0.0, climb = 0.0;

	DEBUG(GYPSY_COURSE": GetCourse: Notify");

	dbus_g_proxy_end_call(proxy, call, &error,
		G_TYPE_INT, &fields,
		G_TYPE_INT, &timestamp,
		G_TYPE_DOUBLE, &speed,
		G_TYPE_DOUBLE, &direction,
		G_TYPE_DOUBLE, &climb,
		G_TYPE_INVALID);

	if (error) {
		WARNING(GYPSY_COURSE": GetCourse: %s", error->message);
		g_error_free(error);
	} else {
		DEBUG(GYPSY_COURSE": GetCourse: OK");

		if (proxy == course_proxy) {
			course_changed(NULL, fields, timestamp, speed,
				direction, climb, NULL);
		}
	}
}

static void
get_satellites_notify(DBusGProxy *proxy, DBusGProxyCall *call, gpointer data)
{
	GError *error = NULL;
	GPtrArray *satellites = NULL;

	DEBUG(GYPSY_SATELLITE": GetSatellites: Notify");

	dbus_g_proxy_end_call(proxy, call, &error,
		dbus_g_type_get_collection("GPtrArray",
			dbus_g_type_get_struct("GValueArray",
				G_TYPE_UINT,
				G_TYPE_BOOLEAN,
				G_TYPE_UINT,
				G_TYPE_UINT,
				G_TYPE_UINT,
				G_TYPE_INVALID)),
		&satellites,
		G_TYPE_INVALID);

	if (error) {
		WARNING(GYPSY_SATELLITE": GetSatellites: %s", error->message);
		g_error_free(error);
	} else {
		DEBUG(GYPSY_SATELLITE": GetSatellites: OK");

		if (proxy == satellite_proxy) {
			satellites_changed(NULL, satellites, NULL);
		}
	}
}

/******************************************************************************/

static void
device_start_notify(DBusGProxy *proxy, DBusGProxyCall *call, gpointer data)
{
	GError *error = NULL;

	DEBUG(GYPSY_DEVICE": Start: Notify");

	dbus_g_proxy_end_call(proxy, call, &error, G_TYPE_INVALID);

	if (error) {
		WARNING(GYPSY_DEVICE": Start: %s", error->message);
		g_error_free(error);
		gypsy_device_start_failed = TRUE;
	} else {
		DEBUG(GYPSY_DEVICE": Start: OK");
		gypsy_device_start_failed = FALSE;

		if (device_proxy) {
			DEBUG(GYPSY_DEVICE": GetConnectionStatus");
			dbus_g_proxy_begin_call(device_proxy, "GetConnectionStatus",
				get_connection_status_notify, NULL, NULL,
				G_TYPE_INVALID);

			DEBUG(GYPSY_DEVICE": GetFixStatus");
			dbus_g_proxy_begin_call(device_proxy, "GetFixStatus",
				get_fix_status_notify, NULL, NULL,
				G_TYPE_INVALID);
		}

		if (time_proxy) {
			DEBUG(GYPSY_TIME": GetTime");
			dbus_g_proxy_begin_call(time_proxy, "GetTime",
				get_time_notify, NULL, NULL,
				G_TYPE_INVALID);
		}

		if (position_proxy) {
			DEBUG(GYPSY_POSITION": GetPosition");
			dbus_g_proxy_begin_call(position_proxy, "GetPosition",
				get_position_notify, NULL, NULL,
				G_TYPE_INVALID);
		}

		if (accuracy_proxy) {
			DEBUG(GYPSY_ACCURACY": GetAccuracy");
			dbus_g_proxy_begin_call(accuracy_proxy, "GetAccuracy",
				get_accuracy_notify, NULL, NULL,
				G_TYPE_INVALID);
		}

		if (course_proxy) {
			DEBUG(GYPSY_COURSE": GetCourse");
			dbus_g_proxy_begin_call(course_proxy, "GetCourse",
				get_course_notify, NULL, NULL,
				G_TYPE_INVALID);
		}

		if (satellite_proxy) {
			DEBUG(GYPSY_SATELLITE": GetSatellites");
			dbus_g_proxy_begin_call(satellite_proxy, "GetSatellites",
				get_satellites_notify, NULL, NULL,
				G_TYPE_INVALID);
		}
	}
}

/******************************************************************************/

static void
device_start(void)
{
	if (!gypsy_device_path) {
		WARNING("No device, not starting");
		return;
	}

	gypsy_device_start_failed = FALSE;

	/* Device */
	device_proxy = dbus_g_proxy_new_for_name(connection,
		gypsy_service, gypsy_device_path, GYPSY_DEVICE);

	DEBUG(GYPSY_DEVICE": Start");
	dbus_g_proxy_begin_call(device_proxy, "Start",
		device_start_notify, NULL, NULL,
		G_TYPE_INVALID);

	dbus_g_proxy_add_signal(device_proxy, "ConnectionStatusChanged",
		G_TYPE_BOOLEAN,
		G_TYPE_INVALID);
	dbus_g_proxy_connect_signal(device_proxy, "ConnectionStatusChanged",
		G_CALLBACK(connection_status_changed), NULL, NULL);

	dbus_g_proxy_add_signal(device_proxy, "FixStatusChanged",
		G_TYPE_INT,
		G_TYPE_INVALID);
	dbus_g_proxy_connect_signal(device_proxy, "FixStatusChanged",
		G_CALLBACK(fix_status_changed), NULL, NULL);

	/* Time */
	time_proxy = dbus_g_proxy_new_for_name(connection,
		gypsy_service, gypsy_device_path, GYPSY_TIME);

	dbus_g_proxy_add_signal(time_proxy, "TimeChanged",
		G_TYPE_INT,
		G_TYPE_INVALID);
	dbus_g_proxy_connect_signal(time_proxy, "TimeChanged",
		G_CALLBACK(time_changed), NULL, NULL);

	/* Position */
	position_proxy = dbus_g_proxy_new_for_name(connection,
		gypsy_service, gypsy_device_path, GYPSY_POSITION);

	dbus_g_proxy_add_signal(position_proxy, "PositionChanged",
		G_TYPE_INT,
		G_TYPE_INT,
		G_TYPE_DOUBLE,
		G_TYPE_DOUBLE,
		G_TYPE_DOUBLE,
		G_TYPE_INVALID);
	dbus_g_proxy_connect_signal(position_proxy, "PositionChanged",
		G_CALLBACK(position_changed), NULL, NULL);

	/* Accuracy */
	accuracy_proxy = dbus_g_proxy_new_for_name(connection,
		gypsy_service, gypsy_device_path, GYPSY_ACCURACY);

	dbus_g_proxy_add_signal(accuracy_proxy, "AccuracyChanged",
		G_TYPE_INT,
		G_TYPE_DOUBLE,
		G_TYPE_DOUBLE,
		G_TYPE_DOUBLE,
		G_TYPE_INVALID);
	dbus_g_proxy_connect_signal(accuracy_proxy, "AccuracyChanged",
		G_CALLBACK(accuracy_changed), NULL, NULL);

	/* Course */
	course_proxy = dbus_g_proxy_new_for_name(connection,
		gypsy_service, gypsy_device_path, GYPSY_COURSE);

	dbus_g_proxy_add_signal(course_proxy, "CourseChanged",
		G_TYPE_INT,
		G_TYPE_INT,
		G_TYPE_DOUBLE,
		G_TYPE_DOUBLE,
		G_TYPE_DOUBLE,
		G_TYPE_INVALID);
	dbus_g_proxy_connect_signal(course_proxy, "CourseChanged",
		G_CALLBACK(course_changed), NULL, NULL);

	/* Satellites */
	satellite_proxy = dbus_g_proxy_new_for_name(connection,
		gypsy_service, gypsy_device_path, GYPSY_SATELLITE);

	dbus_g_proxy_add_signal(satellite_proxy, "SatellitesChanged",
		dbus_g_type_get_collection("GPtrArray",
			dbus_g_type_get_struct("GValueArray",
				G_TYPE_UINT,
				G_TYPE_BOOLEAN,
				G_TYPE_UINT,
				G_TYPE_UINT,
				G_TYPE_UINT,
				G_TYPE_INVALID)),
		G_TYPE_INVALID);
	dbus_g_proxy_connect_signal(satellite_proxy, "SatellitesChanged",
		G_CALLBACK(satellites_changed), NULL, NULL);
}

/******************************************************************************/

static void
device_stop_notify(DBusGProxy *proxy, DBusGProxyCall *call, gpointer data)
{
	GError *error = NULL;

	DEBUG(GYPSY_DEVICE": Stop: Notify");

	dbus_g_proxy_end_call(proxy, call, &error, G_TYPE_INVALID);

	if (error) {
		WARNING(GYPSY_DEVICE": Stop: %s", error->message);
		g_error_free(error);
	} else {
		DEBUG(GYPSY_DEVICE": Stop: OK");
	}
}

static void
device_stop(void)
{
	if (!gypsy_device_path) {
		WARNING("No device, not stopping");
		return;
	}

	/* Device */
	if (gypsy_device_start_failed) {
		gypsy_device_start_failed = FALSE;
	} else {
		DEBUG(GYPSY_DEVICE": Stop");
		dbus_g_proxy_begin_call(device_proxy, "Stop",
			device_stop_notify, NULL, NULL,
			G_TYPE_INVALID);
	}

	dbus_g_proxy_disconnect_signal(device_proxy, "ConnectionStatusChanged",
		G_CALLBACK(connection_status_changed), NULL);
	dbus_g_proxy_disconnect_signal(device_proxy, "FixStatusChanged",
		G_CALLBACK(fix_status_changed), NULL);
	g_object_unref(G_OBJECT(device_proxy));
	device_proxy = NULL;
	device_connectionstatus = FALSE;
	device_fixstatus = 0;

	/* Time */
	dbus_g_proxy_disconnect_signal(time_proxy, "TimeChanged",
		G_CALLBACK(time_changed), NULL);
	g_object_unref(G_OBJECT(time_proxy));
	time_proxy = NULL;
	time_seconds = 0;

	/* Position */
	dbus_g_proxy_disconnect_signal(position_proxy, "PositionChanged",
		G_CALLBACK(position_changed), NULL);
	g_object_unref(G_OBJECT(position_proxy));
	position_proxy = NULL;
	position_fields = 0;
	position_timestamp = 0;
	position_latitude = 0.0;
	position_longitude = 0.0;
	position_altitude = 0.0;

	/* Accuracy */
	dbus_g_proxy_disconnect_signal(accuracy_proxy, "AccuracyChanged",
		G_CALLBACK(accuracy_changed), NULL);
	g_object_unref(G_OBJECT(accuracy_proxy));
	accuracy_proxy = NULL;
	accuracy_fields = 0;
	accuracy_pdop = 0.0;
	accuracy_hdop = 0.0;
	accuracy_vdop = 0.0;

	/* Course */
	dbus_g_proxy_disconnect_signal(course_proxy, "CourseChanged",
		G_CALLBACK(course_changed), NULL);
	g_object_unref(G_OBJECT(course_proxy));
	course_proxy = NULL;
	course_fields = 0;
	course_timestamp = 0;
	course_speed = 0.0;
	course_direction = 0.0;
	course_climb = 0.0;

	/* Satellite */
	dbus_g_proxy_disconnect_signal(satellite_proxy, "SatellitesChanged",
		G_CALLBACK(satellites_changed), NULL);
	g_object_unref(G_OBJECT(satellite_proxy));
	satellite_proxy = NULL;
	satellite_satellitec = 0;
}

/******************************************************************************/

static void
server_create_notify(DBusGProxy *proxy, DBusGProxyCall *call, gpointer data)
{
	GError *error = NULL;
	const char *path = NULL;

	DEBUG(GYPSY_SERVER": Create: Notify");

	dbus_g_proxy_end_call(proxy, call, &error,
		DBUS_TYPE_G_OBJECT_PATH, &path,
		G_TYPE_INVALID);

	if (error) {
		WARNING(GYPSY_SERVER": Create: %s", error->message);
		g_error_free(error);
		gypsy_device_create_failed = TRUE;
		gypsy_device_path = NULL;
	} else {
		DEBUG(GYPSY_SERVER": Create: OK: %s", path);
		gypsy_device_create_failed = FALSE;
		gypsy_device_path = g_strdup(path);

		if (client_list || nowait) {
			device_start();
		}
	}
}

static void
server_create(void)
{
	gypsy_device_create_failed = FALSE;

	DEBUG(GYPSY_SERVER": Create");
	dbus_g_proxy_begin_call(server_proxy, "Create",
		server_create_notify, NULL, NULL,
		G_TYPE_STRING, gypsy_device,
		G_TYPE_INVALID);
}

/******************************************************************************/

static void
name_owner_changed(
	DBusGProxy *proxy,
	const char *name,
	const char *prev,
	const char *new,
	gpointer user_data
)
{
	INFO("org.freedesktop.DBus: NameOwnerChanged(name=%s, prev=%s, new=%s)",
		name, prev, new);

	if (name && !strcmp(name, gypsy_service)) {
		if (prev && strlen(prev) > 0) {
			g_free(gypsy_device_path);
		}
		if (new && strlen(new) > 0) {
			server_create();
		}
		gypsy_device_path = NULL;
	}
}

/******************************************************************************/

static gboolean
timeout_handler(gpointer data)
{
	if (!client_list && !nowait) {
		DEBUG("Timeout: no client: removing timeout handler");
		timeout_handler_registered = FALSE;
		return FALSE;
	} else {
		if (gypsy_device_create_failed) {
			WARNING("Timeout: no gypsy device created");
			server_create();
		} else if (!gypsy_device_path) {
			WARNING("Timeout: no gypsy device available");
		} else if (gypsy_device_start_failed) {
			WARNING("Timeout: gypsy device not started");
			DEBUG(GYPSY_DEVICE": Start");
			dbus_g_proxy_begin_call(device_proxy, "Start",
				device_start_notify, NULL, NULL,
				G_TYPE_INVALID);
		} else if (!device_connectionstatus) {
			WARNING("Timeout: device started but not connected");
		} else {
			GTimeVal tv;

			DEBUG("Timeout: sending data to clients");

			g_get_current_time(&tv);

			send_info_to_clients(&tv);
			send_sats_to_clients(&tv);
			send_nmea_to_clients(&tv);
		}
		return TRUE;
	}
}

/******************************************************************************/

static void
marshal_VOID__STRING_STRING_STRING(
	GClosure *closure,
	GValue *return_value G_GNUC_UNUSED,
	guint n_param_values,
	const GValue *param_values,
	gpointer invocation_hint G_GNUC_UNUSED,
	gpointer marshal_data
)
{
	typedef void (*GMarshalFunc_VOID__STRING_STRING_STRING)(
		gpointer data1,
		const gchar *arg_1,
		const gchar *arg_2,
		const gchar *arg_3,
		gpointer data2);

	GMarshalFunc_VOID__STRING_STRING_STRING callback;
	GCClosure *cc = (GCClosure*) closure;
	gpointer data1, data2;

	g_return_if_fail (n_param_values == 4);

	if (G_CCLOSURE_SWAP_DATA(closure)) {
		data1 = closure->data;
		data2 = g_value_peek_pointer(param_values + 0);
	} else {
		data1 = g_value_peek_pointer(param_values + 0);
		data2 = closure->data;
	}

	callback = (GMarshalFunc_VOID__STRING_STRING_STRING) (
		marshal_data ? marshal_data : cc->callback);

	callback(data1,
		(const gchar *) g_value_get_string(param_values + 1),
		(const gchar *) g_value_get_string(param_values + 2),
		(const gchar *) g_value_get_string(param_values + 3),
		data2);
}

static void
marshal_VOID__INT_INT_DOUBLE_DOUBLE_DOUBLE(
	GClosure *closure,
	GValue *return_value G_GNUC_UNUSED,
	guint n_param_values,
	const GValue *param_values,
	gpointer invocation_hint G_GNUC_UNUSED,
	gpointer marshal_data
)
{
	typedef void (*GMarshalFunc_VOID__INT_INT_DOUBLE_DOUBLE_DOUBLE)(
		gpointer data1,
		gint arg_1,
		gint arg_2,
		gdouble arg_3,
		gdouble arg_4,
		gdouble arg_5,
		gpointer data2);

	GMarshalFunc_VOID__INT_INT_DOUBLE_DOUBLE_DOUBLE callback;
	GCClosure *cc = (GCClosure*) closure;
	gpointer data1, data2;

	g_return_if_fail(n_param_values == 6);

	if (G_CCLOSURE_SWAP_DATA(closure)) {
		data1 = closure->data;
		data2 = g_value_peek_pointer(param_values + 0);
	} else {
		data1 = g_value_peek_pointer(param_values + 0);
		data2 = closure->data;
	}

	callback = (GMarshalFunc_VOID__INT_INT_DOUBLE_DOUBLE_DOUBLE) (
		marshal_data ? marshal_data : cc->callback);

	callback(data1,
		g_value_get_int(param_values + 1),
		g_value_get_int(param_values + 2),
		g_value_get_double(param_values + 3),
		g_value_get_double(param_values + 4),
		g_value_get_double(param_values + 5),
		data2);
}

static void
marshal_VOID__INT_DOUBLE_DOUBLE_DOUBLE(
	GClosure *closure,
	GValue *return_value G_GNUC_UNUSED,
	guint n_param_values,
	const GValue *param_values,
	gpointer invocation_hint G_GNUC_UNUSED,
	gpointer marshal_data
)
{
	typedef void (*GMarshalFunc_VOID__INT_DOUBLE_DOUBLE_DOUBLE)(
		gpointer data1,
		gint arg_1,
		gdouble arg_2,
		gdouble arg_3,
		gdouble arg_4,
		gpointer data2);

	GMarshalFunc_VOID__INT_DOUBLE_DOUBLE_DOUBLE callback;
	GCClosure *cc = (GCClosure*) closure;
	gpointer data1, data2;

	g_return_if_fail(n_param_values == 5);

	if (G_CCLOSURE_SWAP_DATA(closure)) {
		data1 = closure->data;
		data2 = g_value_peek_pointer(param_values + 0);
	} else {
		data1 = g_value_peek_pointer(param_values + 0);
		data2 = closure->data;
	}

	callback = (GMarshalFunc_VOID__INT_DOUBLE_DOUBLE_DOUBLE) (
		marshal_data ? marshal_data : cc->callback);

	callback(data1,
		g_value_get_int(param_values + 1),
		g_value_get_double(param_values + 2),
		g_value_get_double(param_values + 3),
		g_value_get_double(param_values + 4),
		data2);
}

/******************************************************************************/

static gboolean
read_from_client(GIOChannel *source, GIOCondition condition, gpointer data)
{
	struct client *client;
	char buf[4096];
	gsize len = sizeof(buf);
	GIOStatus status;
	GString *reply;
	char *p;

	client = data;

	if (condition != G_IO_IN) {
		shutdown_client(client);
	}

	if (!client->channel) {
		MESSAGE("Removing client");

		client_list = g_list_remove(client_list, client);
		g_free(client);
		g_io_channel_unref(source);

		if (!client_list && !nowait) {
			device_stop();
		}

		return FALSE;
	}

	status = g_io_channel_read_chars(source, buf, len - 2, &len, NULL);

	if ((status != G_IO_STATUS_NORMAL) || (len < 1)) {
		shutdown_client(client);
		return TRUE;
	}

	if (buf[len - 1] != '\n') {
		buf[len] = '\n';
		len++;
	}

	buf[len] = '\0';

	reply = g_string_sized_new(128);
	g_string_assign(reply, "GPSD");

	p = buf;

	while (*p) {
		switch (toupper(*p++)) {
		case 'A':
			if (!POSITION_ALTITUDE_IS_VALID) {
				g_string_append(reply, ",A=?");
			} else {
				g_string_append_printf(reply, ",A=%.3f",
					position_altitude);
			}
			break;
		case 'B':
			g_string_append(reply, ",B=?");
			break;
		case 'C':
			g_string_append(reply, ",C=1.00"); /* FIXME */
			break;
		case 'D':
			if (time_seconds <= 0) {
				g_string_append(reply, ",D=?");
			} else {
				struct tm *tm;
				time_t seconds;

				seconds = time_seconds;
				tm = gmtime(&seconds);

				g_string_append_printf(reply,
					",D=%04d-%02d-%02dT%02d:%02d:%02d.00Z",
					tm->tm_year + 1900, tm->tm_mon + 1,
					tm->tm_mday, tm->tm_hour, tm->tm_min,
					tm->tm_sec);
			}
			break;
		case 'E':
			g_string_append(reply, ",E=");

			if (!ACCURACY_PDOP_IS_VALID) {
				g_string_append(reply, "?");
			} else {
				g_string_append_printf(reply, "%.2f",
					accuracy_pdop);
			}
			if (!ACCURACY_HDOP_IS_VALID) {
				g_string_append(reply, " ?");
			} else {
				g_string_append_printf(reply, " %.2f",
					accuracy_hdop);
			}
			if (!ACCURACY_VDOP_IS_VALID) {
				g_string_append(reply, " ?");
			} else {
				g_string_append_printf(reply, " %.2f",
					accuracy_vdop);
			}
			break;
		case 'F':
			if (*p == '=') {
				while (isprint(*p) && !isspace(*p)) {
					p++;
				}
			}
			g_string_append(reply, ",F=gypsy");
			break;
		case 'G':
			if (*p == '=') {
				p += strcspn(p, ",\r\n");
			}
			g_string_append(reply, ",G=GPS");
			break;
		case 'I':
			g_string_append(reply, ",I=gypsy");
			break;
		case 'J':
			if (*p == '=') {
				p++;
			}
			if (*p == '1' || *p == '+') {
				p++;
				g_string_append(reply, ",J=1");
			} else {
				if (*p == '0' || *p == '-') {
					p++;
				}
				g_string_append(reply, ",J=0");
			}
			break;
		case 'K':
			g_string_append(reply, ",K=1 gypsy");
			break;
		case 'L':
			g_string_append(reply, ",L=3 " VERSION
				" abcdefgijklmnopqrstuvwxyz");
			break;
		case 'M':
			g_string_append_printf(reply, ",M=%d",
				get_fixstatus());
			break;
		case 'N':
			g_string_append(reply, ",N=0");
			break;
		case 'O':
			add_info(reply);
			break;
		case 'P':
			if (!POSITION_LATITUDE_IS_VALID
			 || !POSITION_LONGITUDE_IS_VALID) {
				g_string_append(reply, ",P=?");
			} else {
				g_string_append_printf(reply, ",P=%.6f %.6f",
					position_latitude, position_longitude);
			}
			break;
		case 'Q':
			if (!ACCURACY_PDOP_IS_VALID
			 && !ACCURACY_HDOP_IS_VALID
			 && !ACCURACY_VDOP_IS_VALID) {
				g_string_append(reply, ",Q=?");
			} else {
				unsigned i, used;

				for (i = 0, used = 0; i < satellite_satellitec; i++) {
					if (satellite_satellitev[i].used) {
						used++;
					}
				}

				g_string_append_printf(reply,
					",Q=%d %.2f %.2f %.2f %.2f %.2f", used,
					ACCURACY_PDOP_IS_VALID ? accuracy_pdop : 0.0,
					ACCURACY_HDOP_IS_VALID ? accuracy_hdop : 0.0,
					ACCURACY_VDOP_IS_VALID ? accuracy_vdop : 0.0,
					0.0, 0.0);
			}
			break;
		case 'R':
			if (*p == '=') {
				p++;
			}
			if (*p == '2' || *p == '1' || *p == '+') {
				client->raw = TRUE;
				p++;
			} else if (*p == '0' || *p == '-') {
				client->raw = FALSE;
				p++;
			} else {
				client->raw = !client->raw;
			}
			g_string_append_printf(reply, ",R=%1d", client->raw);
			break;
		case 'S':
			g_string_append_printf(reply, ",S=%d",
				MAX(get_fixstatus() - 1, 0));
			break;
		case 'T':
			if (!COURSE_DIRECTION_IS_VALID) {
				g_string_append(reply, ",T=?");
			} else {
				g_string_append_printf(reply, ",T=%.4f",
					course_direction);
			}
			break;
		case 'U':
			if (!COURSE_CLIMB_IS_VALID) {
				g_string_append(reply, ",U=?");
			} else {
				g_string_append_printf(reply, ",U=%.3f",
					course_climb);
			}
			break;
		case 'V':
			if (!COURSE_SPEED_IS_VALID) {
				g_string_append(reply, ",V=?");
			} else {
				g_string_append_printf(reply, ",V=%.3f",
					course_speed * KNOTS_TO_MPS);
			}
			break;
		case 'W':
			if (*p == '=') {
				p++;
			}
			if (*p == '1' || *p == '+') {
				client->watcher = TRUE;
				p++;
			} else if (*p == '0' || *p == '-') {
				client->watcher = FALSE;
				p++;
			} else {
				client->watcher = !client->watcher;
			}
			g_string_append_printf(reply, ",W=%1d", client->watcher);
			break;
		case 'X':
			if (time_seconds <= 0) {
				g_string_append(reply, ",X=?");
			} else {
				g_string_append_printf(reply, ",X=%d.000000",
					time_seconds);
			}
			break;
		case 'Y':
			add_sats(reply);
			break;
		case 'Z':
			g_string_append(reply, ",Z=?"); /* FIXME */
			break;
		case '$':
			g_string_append(reply, ",$=?"); /* FIXME */
			break;
		case '\r':
		case '\n':
			goto finish;
		default:
			break;
		}
	}

finish:
	g_string_append(reply, "\r\n");
	send_to_client(client, reply->str, reply->len);
	g_string_free(reply, TRUE);
	return TRUE;
}

static gboolean
accept_new_client(GIOChannel *source, GIOCondition condition, gpointer data)
{
	struct sockaddr_in addr;
	socklen_t len = sizeof(addr);
	struct client *client;
	int fd;

	MESSAGE("New client request");

	fd = accept(g_io_channel_unix_get_fd(source),
		(struct sockaddr *) &addr, &len);

	if (fd < 0) {
		ERROR("Cannot accept new connection: %s", strerror(errno));
	}

	client = g_new0(struct client, 1);

	if (!client) {
		ERROR("Cannot allocate memory");
	}

	if (!timeout_handler_registered) {
		timeout_handler_registered = TRUE;
		g_timeout_add(2000, timeout_handler, NULL);
	}

	if (!client_list && !nowait) {
		device_start();
	}

	client_list = g_list_prepend(client_list, client);

	client->channel = g_io_channel_unix_new(fd);

	g_io_channel_set_flags(client->channel, G_IO_FLAG_NONBLOCK, NULL);
	g_io_channel_set_encoding(client->channel, NULL, NULL);
	g_io_channel_set_buffered(client->channel, FALSE);

	g_io_add_watch(client->channel,
		G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP | G_IO_NVAL,
		read_from_client, client);

	return TRUE;
}

static void
init_tcp_service(const char *address, const char *port)
{
	struct addrinfo hints, *l, *list = NULL;
	int error, errno_backup = 0;
	gboolean ok = FALSE;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	if (address && !strlen(address)) {
		address = NULL;
	}

	if (!port || !strlen(port)) {
		port = "2947";
	}

	error = getaddrinfo(address, port, &hints, &list);

	if (error) {
		if (address) {
			ERROR("Cannot get address info for address=%s, port=%s: %s",
				address, port, gai_strerror(error));
		} else {
			ERROR("Cannot get address info for port=%s: %s",
				port, gai_strerror(error));
		}
	}

	for (l = list; l; l = l->ai_next) {
		GIOChannel *channel;
		int fd, one = 1;

		fd = socket(l->ai_family, l->ai_socktype, l->ai_protocol);

		if (fd < 0) {
			errno_backup = errno;
			continue;
		}

		setsockopt(fd, SOL_SOCKET, SO_REUSEADDR,
			(void *) &one, sizeof(one));

		if (bind(fd, l->ai_addr, l->ai_addrlen) < 0) {
			errno_backup = errno;
			close(fd);
			continue;
		}

		if (listen(fd, 4) < 0) {
			errno_backup = errno;
			close(fd);
			continue;
		}

		channel = g_io_channel_unix_new(fd);

		g_io_channel_set_encoding(channel, NULL, NULL);
		g_io_channel_set_buffered(channel, FALSE);

		g_io_add_watch(channel, G_IO_IN | G_IO_PRI | G_IO_HUP | G_IO_ERR,
			accept_new_client, NULL);

		ok = TRUE;
	}

	if (!ok) {
		if (address) {
			ERROR("Cannot listen on address=%s, port=%s: %s",
				address, port, strerror(errno_backup));
		} else {
			ERROR("Cannot listen on port=%s: %s",
				port, strerror(errno_backup));
		}
	}

	if (list) {
		freeaddrinfo(list);
	}
}

/******************************************************************************/

static void
printerr_handler(const char *message)
{
	if (background) {
		syslog(LOG_USER | LOG_INFO, "%s", message);
	} else {
		fputs(message, stderr);
	}
}

static void
log_handler(
	const char *domain,
	GLogLevelFlags level,
	const char *message,
	gpointer user_data
)
{
	const char *level_string;

	switch (level & G_LOG_LEVEL_MASK) {
	case G_LOG_LEVEL_ERROR:
		level_string = "ERROR: ";
		break;
	case G_LOG_LEVEL_CRITICAL:
		level_string = "CRITICAL: ";
		break;
	case G_LOG_LEVEL_WARNING:
		level_string = "WARNING: ";
		break;
	case G_LOG_LEVEL_MESSAGE:
		level_string = "MESSAGE: ";
		break;
	case G_LOG_LEVEL_INFO:
		level_string = "INFO: ";
		break;
	case G_LOG_LEVEL_DEBUG:
		level_string = "DEBUG: ";
		break;
	default:
		level_string = "";
		break;
	}

	if (background) {
		if (domain) {
			syslog(LOG_USER | LOG_INFO, "%s: %s%s", domain,
				level_string, message);
		} else {
			syslog(LOG_USER | LOG_INFO, "%s%s",
				level_string, message);
		}
	} else {
		struct timeval tv = { 0, 0 };
		struct tm *tm;
		char buf[64];
		size_t len;

		gettimeofday(&tv, NULL);
		tm = localtime(&tv.tv_sec);
		len = strftime(buf, sizeof(buf), "%T", tm);

		if (!len) {
			*buf = '\0';
		}

		if (domain) {
			fprintf(stderr, "%s.%06li %s: %s%s\n", buf,
				(long) tv.tv_usec, domain, level_string,
				message);
		} else {
			fprintf(stderr, "%s.%06li %s%s\n", buf,
				(long) tv.tv_usec, level_string, message);
		}
	}
}

/******************************************************************************/

static void
usage(void)
{
	printf("\
Usage: "PACKAGE_NAME" [-n] [-N] [-f device] [-P pidfile] [-D n] [-S port] [-h] [-V]\n\
\n\
Options:\n\
  -n                          Don't wait for client connects to poll GPS\n\
  -N                          Don't go into background\n\
  -f device                   Set a GPS device name\n\
  -P pidfile                  Set file to record process ID\n\
  -D integer (default 0)      Set debug level\n\
  -S integer (default 2947)   Set port for daemon\n\
  -h                          Display help message and terminate\n\
  -V                          Emit version and exit\n");
}

/******************************************************************************/

int
main(int argc, char **argv)
{
	DBusGProxy *proxy_dbus = NULL;
	GMainLoop *loop = NULL;
	char *port = NULL;
	char *address = NULL;
	char *pid_file = NULL;
	gboolean foreground = FALSE;
	GError *error = NULL;

	g_set_prgname(PACKAGE_NAME);
	g_set_printerr_handler(printerr_handler);
	g_log_set_default_handler(log_handler, NULL);

	opterr = 0;
	while (1) {
		int option;

		option = getopt(argc, argv, "F:D:S:bhNnP:VR:f:");

		if (option == -1) {
			break;
		}

		switch (option) {
		case 'F':
		case 'b':
		case 'R':
			WARNING("Not implemented: -%c", option);
			break;
		case 'D':
			switch (atoi(optarg)) {
			case 0:
				log_level = G_LOG_LEVEL_ERROR
					  | G_LOG_LEVEL_CRITICAL
					  | G_LOG_LEVEL_WARNING;
				break;
			case 1:
				log_level = G_LOG_LEVEL_ERROR
					  | G_LOG_LEVEL_CRITICAL
					  | G_LOG_LEVEL_WARNING
					  | G_LOG_LEVEL_MESSAGE;
				break;
			case 2:
				log_level = G_LOG_LEVEL_ERROR
					  | G_LOG_LEVEL_CRITICAL
					  | G_LOG_LEVEL_WARNING
					  | G_LOG_LEVEL_MESSAGE
					  | G_LOG_LEVEL_INFO;
				break;
			default:
				log_level = G_LOG_LEVEL_ERROR
					  | G_LOG_LEVEL_CRITICAL
					  | G_LOG_LEVEL_WARNING
					  | G_LOG_LEVEL_MESSAGE
					  | G_LOG_LEVEL_INFO
					  | G_LOG_LEVEL_DEBUG;
				break;
			}
			break;
		case 'f':
			gypsy_device = optarg;
			break;
		case 'N':
			foreground = TRUE;
			break;
		case 'S':
			port = strrchr(optarg, ':');
			if (!port) {
				port = optarg;
				address = NULL;
			} else {
				port[0] = '\0';
				port++;
				address = optarg;
			}
			break;
		case 'n':
			nowait = TRUE;
			break;
		case 'P':
			pid_file = optarg;
			break;
		case 'V':
			printf(PACKAGE_NAME" "VERSION"\n");
			exit(EXIT_SUCCESS);
			break;
		case 'h':
		case '?':
		default:
			usage();
			exit(EXIT_SUCCESS);
			break;
		}
	}

	if (!foreground) {
		pid_t pid;
		int fd;

		pid = fork();

		if (pid < 0) {
			ERROR("Cannot fork: %s", strerror(errno));
		} else if (pid > 0) {
			exit(EXIT_SUCCESS);
		}

		openlog(PACKAGE_NAME, LOG_PID, LOG_USER);
		background = TRUE;

		setsid();

		if (chdir("/")) {
			ERROR("Cannot chdir: /: %s", strerror(errno));
		}

		fd = open("/dev/null", O_RDWR, 0);

		if (fd < 0) {
			ERROR("Cannot open: /dev/null: %s", strerror(errno));
		}

		dup2(fd, STDIN_FILENO);
		dup2(fd, STDOUT_FILENO);
		dup2(fd, STDERR_FILENO);

		close(fd);
	}

	{
		char *s;

		s = getenv("FSO_GPSD_GYPSY_SERVICE");

		if (s) {
			WARNING("Found environment variable: "
				"FSO_GPSD_GYPSY_SERVICE = %s", s);
			gypsy_service = g_strdup(s);
		}

		s = getenv("FSO_GPSD_GYPSY_SERVER_PATH");

		if (s) {
			WARNING("Found environment variable: "
				"FSO_GPSD_GYPSY_SERVER_PATH = %s", s);
			gypsy_server_path = g_strdup(s);
		}
	}

	if (pid_file) {
		FILE *fp;

		fp = fopen(pid_file, "w");

		if (!fp) {
			ERROR("Cannot fopen: %s: %s", pid_file, strerror(errno));
		}

		fprintf(fp, "%u\n", (unsigned) getpid());
		fclose(fp);
	}

	g_type_init();

	loop = g_main_loop_new(NULL, FALSE);

	connection = dbus_g_bus_get(DBUS_BUS_SYSTEM, &error);

	if (!connection) {
		ERROR("Failed to open connection to system bus: %s",
			error->message);
	}

	dbus_g_object_register_marshaller(
		marshal_VOID__STRING_STRING_STRING,
		G_TYPE_NONE,
		G_TYPE_STRING,
		G_TYPE_STRING,
		G_TYPE_STRING,
		G_TYPE_INVALID);
	dbus_g_object_register_marshaller(
		marshal_VOID__INT_DOUBLE_DOUBLE_DOUBLE,
		G_TYPE_NONE,
		G_TYPE_INT,
		G_TYPE_DOUBLE,
		G_TYPE_DOUBLE,
		G_TYPE_DOUBLE,
		G_TYPE_INVALID);
	dbus_g_object_register_marshaller(
		marshal_VOID__INT_INT_DOUBLE_DOUBLE_DOUBLE,
		G_TYPE_NONE,
		G_TYPE_INT,
		G_TYPE_INT,
		G_TYPE_DOUBLE,
		G_TYPE_DOUBLE,
		G_TYPE_DOUBLE,
		G_TYPE_INVALID);

	proxy_dbus = dbus_g_proxy_new_for_name(connection,
		"org.freedesktop.DBus", "/org/freedesktop/DBus",
		"org.freedesktop.DBus");

	server_proxy = dbus_g_proxy_new_for_name(connection,
		gypsy_service, gypsy_server_path, GYPSY_SERVER);

	dbus_g_proxy_add_signal(proxy_dbus, "NameOwnerChanged",
		G_TYPE_STRING,
		G_TYPE_STRING,
		G_TYPE_STRING,
		G_TYPE_INVALID);
	dbus_g_proxy_connect_signal(proxy_dbus, "NameOwnerChanged",
		G_CALLBACK(name_owner_changed), NULL, NULL);

	server_create();

	nmea_gga = g_string_sized_new(128);
	nmea_gsa = g_string_sized_new(128);
	nmea_gsv[0] = g_string_sized_new(128);
	nmea_gsv[1] = g_string_sized_new(128);
	nmea_gsv[2] = g_string_sized_new(128);
	nmea_rmc = g_string_sized_new(128);
	nmea_gll = g_string_sized_new(128);
	nmea_vtg = g_string_sized_new(128);

	init_tcp_service(address, port);

	if (nowait) {
		timeout_handler_registered = TRUE;
		g_timeout_add(2000, timeout_handler, NULL);
	}

	g_main_loop_run(loop);

	return 0;
}

